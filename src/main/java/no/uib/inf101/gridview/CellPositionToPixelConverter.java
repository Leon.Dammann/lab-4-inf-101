
package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

import java.awt.geom.Rectangle2D;

public class CellPositionToPixelConverter {
    Rectangle2D box;
    GridDimension gd;
    double margin;

    public CellPositionToPixelConverter(Rectangle2D rectangle, GridDimension gd, double margin){
        this.box = rectangle;
        this.gd = gd;
        this.margin = margin;
    }

    public Rectangle2D getBoundsForCell(CellPosition cp) {
        double Width = (box.getWidth() - (double)(gd.cols()+1) * margin) / (double)(gd.cols());
        double Height = (box.getHeight() - (double)(gd.rows()+1) * margin) / (double)(gd.rows());

        double X = box.getX() + margin + cp.col() * (margin + Width);
        double Y = box.getY() + margin + cp.row() * (margin + Height);

        return new Rectangle2D.Double(X, Y, Width, Height);
    }
}