package no.uib.inf101.gridview;

import no.uib.inf101.colorgrid.*;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;

public class GridView extends JPanel{
  IColorGrid colorGrid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid colorGrid){
    this.setPreferredSize(new Dimension(400, 300));
    this.colorGrid = colorGrid;
  }
  @Override
  public void paintComponent(Graphics g){
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;
    drawGrid(g2);
  }
  public void drawGrid(Graphics2D g2d){
    double margin = GridView.OUTERMARGIN;
    double x = margin;
    double y = margin;
    double width = this.getWidth() - 2 * margin;
    double height = this.getHeight() - 2 * margin;
    Rectangle2D rectangle = new Rectangle2D.Double(x, y, width, height);
    g2d.setColor(MARGINCOLOR);
    g2d.fill(rectangle);
    CellPositionToPixelConverter toPixelConverter = new CellPositionToPixelConverter(rectangle, this.colorGrid, margin);
    drawCells(g2d, colorGrid, toPixelConverter);
}
private static void drawCells(Graphics2D g2d, CellColorCollection cellColorCollection, CellPositionToPixelConverter cellPositionToPixelConverter){
    for (CellColor cell: cellColorCollection.getCells()) {
        Rectangle2D rectangle = cellPositionToPixelConverter.getBoundsForCell(cell.cellPosition());
        Color color = cell.color();
        if (cell.color() == null) {
            color = Color.GRAY;
        }
        g2d.setColor(color);
        g2d.fill(rectangle);
    }
}
  
}