package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid{
  private final int rows;
  private final int cols;
  List<List<Color>> grid;

  public ColorGrid (int rows, int cols){
      this.cols = cols;
      this.rows = rows;

      grid = new ArrayList<>(rows);
      for (int i = 0; i < rows; i++) {
        grid.add(new ArrayList<>(cols));
        for(int j = 0; j < cols; j++){
          grid.get(i).add(null);
        }
      }
  }


  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {
    List<CellColor> cellsList = new ArrayList<>();
    for (int i = 0; i < this.rows; i++) {
      for (int j = 0; j < this.cols; j++) {
          cellsList.add(new CellColor(new CellPosition(i, j), grid.get(i).get(j)));
      }
  }
  return cellsList;
  }

  @Override
  public Color get(CellPosition pos) {
    if(pos.row() > this.rows){
      throw new IndexOutOfBoundsException("out of bounds");
    }
    else if(pos.col() > this.cols){
      throw new IndexOutOfBoundsException("out of bounds"); 
    }
    return this.grid.get(pos.row()).get(pos.col());
  }

  @Override
  public void set(CellPosition pos, Color color) {
    this.grid.get(pos.row()).set(pos.col(), color);
    
  }
}
